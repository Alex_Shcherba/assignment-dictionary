%define CUR_ELEMENT 0

%macro colon 2
    %2: dq CUR_ELEMENT
    %define CUR_ELEMENT %2
    db %1, 0
%endmacro

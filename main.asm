%include "words.inc"
%define STR_SIZE 255
%define PTR_SIZE 8

section .data

err: db "К сожалению, такого ключа нет в словаре", 0
stdin_err: db "Ошибка чтения", 0

section .text

global _start

extern exit
extern find_word
extern string_length
extern print_string
extern print_newline
extern read_word
extern string_equals

_start:
    mov     rsi, STR_SIZE
    sub     rsp, STR_SIZE
    mov     rdi, rsp
    call    read_word
    test    rax, rax
    jz      .fail

    mov     rsi, CUR_ELEMENT
    mov     rdi, rax
    call    find_word
    test    rax, rax
    jz      .fail
    
    add     rsp, STR_SIZE

.fail_stdin:
    add     rsp, STR_SIZE 
    mov     rdi, stdin_err
    mov     rsi, 2          ; stderr
    jmp     .end

.success:
    mov     rdi, rax
    add     rdi, PTR_SIZE
    call    string_length
    add     rdi, rax
    inc     rdi
    mov     rsi, 1          ; stdout
    jmp     .end

.fail:
    add     rsp, STR_SIZE 
    mov     rdi, err
    mov     rsi, 2          ; stderr
    jmp     .end

.end:
    call    print_string
    call    print_newline
    call    exit
 

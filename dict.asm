%define PTR_SIZE 8

section .text 
global find_word

extern string_equals

find_word:
    .loop:
        push    rdi
        push    rsi
        add     rsi, PTR_SIZE
        call    string_equals
        pop     rsi
        pop     rdi
        test    rax, rax
        jnz     .exit
        mov     rsi, [rsi]
        test    rsi, rsi
        je      .exit
        jmp     .loop
    
    .exit:
        mov     rax, rsi
        ret       

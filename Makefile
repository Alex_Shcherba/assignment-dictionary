nasm = nasm -felf64 -o 

main:  clean main.o dict.o lib.o 
	ld -o main main.o dict.o lib.o
	
%.o: %.asm
	$(nasm) $@ $<
	
.PHONY: clean
clean:
	rm -f *.o main 
